# Python application for drawing of Fibonacci spiral - using Turtle 

import turtle 
import math 
from turtle import *
from math import *

# MANUAL with commands:
# https://docs.python.org/3.3/library/turtle.html

window=turtle.Screen()
window.bgcolor("white")

print("Fibonacci sequence follows a recursive relation:")
print("next element k = previous + current = i + j")
print("f(n+1) = f(n-1) + f(n)")
print("where n is the n-th term in the sequence.")

# The 'factor' makes possible to expand or shrink the scale of the print.
factor = 3

how_many = int(input('Enter the number of iterations (must be > 1): ')) 

def FibonacciSpiralDraw(n): 
    i_previous=0
    j_current=1
    square_a = i_previous
    square_b = j_current

    # Setting the colour of the plotting pen to grey 
    x.pencolor("grey") 

    # Drawing the first square 
    for f in range(4):
        x.forward(j_current * factor)
        if f < 3 :x.left(90)

    # Proceeding in the Fibonacci Series 
    temp = square_b 
    square_b = square_b + square_a 
    square_a = temp 
    
    # Drawing the rest of the squares 
    for i in range(1, n): 
        x.backward(square_a * factor) 
        x.right(90)
        for f in range(3):
            x.forward(square_b * factor) 
            if f < 2 :x.left(90)

        # Proceeding in the Fibonacci Series 
        temp = square_b 
        square_b = square_b + square_a 
        square_a = temp 

    # Bringing the pen to starting point of the spiral plot 
    x.penup() 
    x.setposition(factor, 0) 
    x.seth(0) 
    x.pendown() 

    # Setting the colour of the plotting pen to red 
    x.pencolor("red") 
    x.pensize(n)
    if n > 3: x.pensize(3)

    # Fibonacci Spiral Plot 
    x.left(90) 
    for i in range(n):  
        fdwd = math.pi * j_current * factor / 2
        fdwd /= 90
        for j in range(90): 
            x.forward(fdwd) 
            x.left(1) 
        temp = i_previous 
        i_previous = j_current 
        j_current = temp + j_current 
        print("Step "+str(i+1)+". i="+str(temp)+", j="+str(i_previous)+", k=Fib="+str(j_current))  

# Drawing the Fibonacci Spiral Fractal 
# and printing the corresponding Fibonacci Number 
if how_many > 0: 
    print("Fibonacci series for", how_many, "elements :") 
    x = turtle.Turtle() 
    x.speed(200) 
    FibonacciSpiralDraw(how_many) 
    turtle.done() 
else: 
    print("Number of iterations must be > 0") 
	